<?php

require_once __DIR__.'/bootstrap.php';
/* @var array $config */
?><html>
<head>
    <script type="text/javascript" src="https://www.2checkout.com/checkout/api/2co.min.js"></script>
</head>
<body>
<h1>Payment</h1>
<div>
    <p>list of CC: https://knowledgecenter.2checkout.com/Documentation/09Test_ordering_system/01Test_payment_methods</p>
    <form id="myCCForm" action="order2.php" method="post">
        <input name="token" type="hidden" value="" />
        <div>
            <label>
                <span>Card Number</span>
                <input id="ccNo" type="text" value="4111111111111111" autocomplete="off" required />
            </label>
        </div>
        <div>
            <label>
                <span>Expiration Date (MM/YYYY)</span>
                <input id="expMonth" type="text" size="2" required value="10" />
            </label>
            <span> / </span>
            <input id="expYear" type="text" size="4" required value="2024" />
        </div>
        <div>
            <label>
                <span>CVC</span>
                <input id="cvv" type="text" value="1234" autocomplete="off" required />
            </label>
        </div>
        <button type="button" id="payment-submit">PAY with 2CO</button>
    </form>
</div>
<script>
    // Called when token created successfully.
    var successCallback = (data) => {
        var myForm = document.getElementById('myCCForm');

        // Set the token as the value for the token input
        myForm.token.value = data.response.token.token;

        console.log('response', data)

        // IMPORTANT: Here we call `submit()` on the form element directly instead of using jQuery to prevent and infinite token request loop.
        myForm.submit();
    };

    // Called when token creation fails.
    var errorCallback = (data) => {
        // Retry the token request if ajax call fails
        if (data.errorCode === 200) {
            console.log('error', data)
            // This error code indicates that the ajax call failed. We recommend that you retry the token request.
        } else {
            alert(data.errorMsg);
        }
    };

    var tokenRequest = function() {
        // Setup token request arguments
        var args = {
            sellerId: "<?php echo $config['account_number']; ?>",
            publishableKey: "<?php echo $config['publishable_key']; ?>",
            ccNo: document.getElementById('ccNo').value,
            cvv: document.getElementById('cvv').value,
            expMonth: document.getElementById('expMonth').value,
            expYear: document.getElementById('expYear').value
        };

        console.log(args);

        // Make the token request
        TCO.requestToken(successCallback, errorCallback, args);
    };

    document.addEventListener("DOMContentLoaded", (event) => {
        console.log('ready');
        TCO.loadPubKey('production', () => {});

        var button = document.getElementById("payment-submit");
        button.addEventListener("click",function(e){
            // Call our token request function
            tokenRequest();

            // Prevent form from submitting
            return false;
        },false);
    });
</script>
</body>
</html>
