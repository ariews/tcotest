<?php

require __DIR__.'/vendor/autoload.php';

(\Dotenv\Dotenv::createImmutable(__DIR__))->load();

$config = [
    'admin' => $_ENV['TCO_ADMIN'],
    'password' => $_ENV['TCO_PASSWORD'],

    'account_number' => $_ENV['TCO_ACCOUNT_NUMBER'],
    'secret' => $_ENV['TCO_SECRET'],

    'publishable_key' => $_ENV['TCO_PUBLISHABLE_KEY'],
    'private_key' => $_ENV['TCO_PRIVATE_KEY'],
];

Twocheckout::username($config['admin']);
Twocheckout::password($config['password']);

Twocheckout::sellerId($config['account_number']);
Twocheckout::privateKey($config['private_key']);
