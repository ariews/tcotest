<?php

declare(strict_types=1);

namespace App\TCO;

class TokenGateway extends \Omnipay\TwoCheckoutPlus\TokenGateway
{
    public function purchase(array $parameters = [])
    {
        return $this->createRequest(TokenPurchaseRequest::class, $parameters);
    }
}
