<?php

declare(strict_types=1);

namespace App\TCO;

class TokenPurchaseRequest extends \Omnipay\TwoCheckoutPlus\Message\TokenPurchaseRequest
{
    public function getEndpoint()
    {
        return $this->liveEndpoint.$this->getAccountNumber().'/rs/authService';
    }

    public function getData()
    {
        $data = parent::getData();

        if ($this->getTestMode()) {
            $data['demo'] = 'Y';
        }

        if (isset($data['billingAddr']) && ! empty($data['billingAddr'])) {
            $data['shippingAddr'] = $data['billingAddr'];
        }

        return $data;
    }
}
