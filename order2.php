<?php

use App\TCO\TokenPurchaseRequest;
use fab2s\SoUuid\SoUuid;
use Omnipay\Common\CreditCard;
use Omnipay\Omnipay;
use Omnipay\TwoCheckoutPlus\Message\TokenPurchaseResponse;

require_once __DIR__.'/bootstrap.php';

echo '<pre>';

print_r($_POST);
/** @var array $config */
$gateway = Omnipay::create('\App\TCO\TokenGateway');

$parameters = [
    'adminUsername' => $config['admin'],
    'adminPassword' => $config['password'],
    'accountNumber' => $config['account_number'],
    'privateKey' => $config['private_key'],
    'secretWord' => $config['secret'],
    'testMode' => true,

    'token' => $_POST['token'],
    'amount' => (string) 1.00,
    'currency' => 'USD',
    'transactionId' => (string) microtime(true),

    // optional card information, use for billing address information
    'card' => new CreditCard([
        'billingFirstName' => 'John',
        'billingLastName' => 'Doe',
        'billingAddress1' => '123 Test St',
        'billingAddress2' => '',
        'billingCity' => 'Columbus',
        'billingState' => 'OH',
        'billingPostcode' => '43123',
        'email' => 'testingtester@2co.com',
        'billingCountry' => 'USA',
        'billingPhone' => '',
        'billingPhoneExtension' => '',
    ]),
];

/** @var TokenPurchaseRequest $request */
$request = $gateway->purchase($parameters);

try {
    print_r([
        'data' => $request->getData(),
        'headers' => $request->getRequestHeaders(),
        'endpoint' => $request->getEndpoint(),
        'json' => json_encode($request->getData()),
    ]);

    /** @var TokenPurchaseResponse $response */
    $response = $request->send();

    print_r($response->getData());
} catch (\Throwable $e) {
}
