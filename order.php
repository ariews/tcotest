<?php

require_once __DIR__.'/bootstrap.php';

echo '<pre>';

print_r($_POST);
/** @var array $config */
$params = [
    'sellerId' => $config['account_number'],
    'privateKey' => $config['private_key'],
    'merchantOrderId' => (string) microtime(true),
    'token' => $_POST['token'],
    'currency' => 'USD',
    'total' => 1.00,
    'demo' => 'Y',
    'billingAddr' => [
        'name' => 'John Doe',
        'addrLine1' => '123 Test St',
        'city' => 'Columbus',
        'state' => 'OH',
        'zipCode' => '43123',
        'country' => 'USA',
        'email' => 'testingtester@2co.com',
        'phoneNumber' => '555-555-5555',
    ],
    'shippingAddr' => [
        'name' => 'John Doe',
        'addrLine1' => '123 Test St',
        'city' => 'Columbus',
        'state' => 'OH',
        'zipCode' => '43123',
        'country' => 'USA',
        'email' => 'testingtester@2co.com',
        'phoneNumber' => '555-555-5555',
    ],
];

try {
    $charge = Twocheckout_Charge::auth($params);
    print_r($charge);
} catch (Twocheckout_Error $error) {
    print_r([
        'code' => $error->getCode(),
        'message' => $error->getMessage(),
    ]);
}
